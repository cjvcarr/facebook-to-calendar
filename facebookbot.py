# -*- coding: utf-8 -*-
from __future__ import print_function

import httplib2
import os
import facebook
import re
import time

from apiclient import discovery
from oauth2client import client, tools
from oauth2client.file import Storage
from datetime import datetime, timedelta
from pytz import timezone

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'C.H Nails calendar app'

def main():
    # authorise with Google Calendar API
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    cal_sdk = discovery.build('calendar', 'v3', http=http)

    # authorise with Facebook Graph API
    key_file = open("keys/facebookkeys.txt","r")
    at = key_file.readline().rstrip()
    pid = key_file.readline().rstrip()
    key_file.close
    graph_api = facebook.GraphAPI(access_token=at, version="3.1")

    # run message scanning function
    print(timestamp() + "Program started")
    messageScanner(graph_api, cal_sdk)

def get_credentials():
    """
    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials..
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')
    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def timestamp():
    timestamp = str(datetime.now()) + ': '
    return timestamp

def getNewConvData(api):
    conv = api.get_object('me/conversations')
    conv_data = conv['data']
    return conv_data

def messageScanner(api, sdk):
    successful_api_calls = 0
    failed_api_calls = 0
    message = {'fields' : 'message'}
    data = {'fields': 'participants, messages'}
    isDay = lambda h : h > 8 and h < 23
    while True: # Running as infinite loop so as not to miss a message
        if isDay(datetime.now().hour):
            try:
                conv_data = getNewConvData(api) # Get new conversation data
                if successful_api_calls == 0:
                    print(timestamp() + 'First successful API call after ' + str(failed_api_calls) + ' failed API calls')
                    failed_api_calls = 0
                successful_api_calls = successful_api_calls + 1
            except:
                if failed_api_calls == 0:
                    print(timestamp() + 'An error occurred after ' + str(successful_api_calls) + ' successful API calls')
                    successful_api_calls = 0
                failed_api_calls = failed_api_calls + 1
                continue
            for i in range(5): # Using range 5 in case multiple conversations are updated at the same time
                try:
                    current_conv = api.get_object(conv_data[i]['id'], **data)
                    client = current_conv['participants']['data'][0]['name']
                except:
                    break
                for u in range(3):  # This loop will scan the latest 3 messages in order newest -> oldest
                    try:            # try/except in case conversation has less than 3 messages
                        msg_key = current_conv['messages']['data'][u]['id']
                        msg_content = api.get_object(msg_key, **message)
                        if checkForConfirmation(msg_content, client, sdk):
                            break   # Break out of loop if a regex match is found (no need to scan next messages)
                    except:
                        pass
                    continue
        time.sleep(60)

def checkForConfirmation(message_content, client, sdk):
    message_body = message_content['message'] # Get only necessary message body text
    regex = r"^(?:[Kk]ohtume|[Pp]an[ei]n kirja|[Nn]äeme|[Oo]led kirjas) [a-zA-Z]*? ?(\d?\d\.\d{2}) ?(?:kell)? (\d?\d\.?\d{2}) (?:geelküünte )?((hooldus)?(paigaldus)?(pealepanek)?(geellakk)?(geeliga ?katmi[ns]e)?)" # regex experssion to match to message_body
    if re.search(regex, message_body):
        match = re.search(regex, message_body)
        if checkCalendarAvailability(match, client, sdk):
            print(timestamp() + u'{} tuleb teenusesse {} kuupäeval {} kell {}'.format(client, match.group(3), match.group(1), match.group(2))) # Print confirmation to console
        return True
    else:
        return False

def checkCalendarAvailability(match, client, sdk):
    teenus = match.group(3) # Meaningful variable names instead of match.group(i)
    if teenus == 'geeligakatmise' or teenus == 'geeligakatmine' or teenus == 'geeliga katmise':
        teenus = 'geeliga katmine' # Change name to be grammatically correct in calendar
    description = client + ' ' + teenus
    date = match.group(1)
    time = match.group(2)
    if (int(date.split('.')[1]) < datetime.now().month):
        year = datetime.now().year+1
    else:
        year = datetime.now().year
    time_object = convertDateToDateTime(year, date, time) # Convert date and time to Datetime object
    start_time = convertDateToISO(time_object)            # Convert Datetime object to ISO formatting
    if teenus in ('pealepanek', 'paigaldus', 'hooldus'): # Set length of different services
        end_time = convertDateToISO(time_object + timedelta(hours=1, minutes=45))
    elif teenus in ('geeliga katmise', 'geeliga katmine'):
        end_time = convertDateToISO(time_object + timedelta(hours=1, minutes=15))
    elif teenus in ('geellakk'):
        end_time = convertDateToISO(time_object + timedelta(hours=1))
    available_query = {"timeMin": start_time,"timeMax": end_time,"timeZone": "Europe/Tallinn","items": [{"id": "primary"}]}
    available_result = sdk.freebusy().query(body=available_query).execute() # Check freebusy to see if timeslot is available
    if not((available_result[u'calendars'][u'primary'])[u'busy']): # If 'busy' is empty, timeslot is available and event can be created
        createCalendarEvent(start_time, end_time, description, sdk)
        return True
    else:
        checkExistingOrOverlap(start_time, end_time, description, sdk)
        return False

def convertDateToDateTime(year, date, time): # EDIT HERE! REGEX EDITED ALREADY
    fmt = '%d.%m.%Y %H.%M'
    time = time.zfill(5)
    date = date.zfill(5)
    datetime_string = ('{}.{} {}'.format(date,year,time))
    datetime_object = datetime.strptime(datetime_string, fmt)
    return datetime_object

def convertDateToISO(datetime_object):
    ee = timezone('Europe/Tallinn')
    datetime_iso = ee.localize(datetime_object).isoformat()
    return datetime_iso

def checkExistingOrOverlap(start, end, description, sdk):
    eventsResult = sdk.events().list(
        calendarId='primary', timeMin=start, timeMax=end, maxResults=1, singleEvents=True,
        orderBy='startTime').execute()
    events = eventsResult.get('items', [])
    if (events[0]['summary']) != description: # Compare the existing event summary to the event we wish to create
        return 'overlap'
    else:
        return 'existing'

def createCalendarEvent(start, end, description, sdk):
    event_body = {
      'summary': description,
      'location': 'C.H Nails geelküüned, Pärnu maantee, Tallinn',
      'description': 'This is an event from the python -> google calendar API',
      'start': {
        'dateTime': start,
        'timeZone': 'Europe/Tallinn',
      },
      'end': {
        'dateTime': end,
        'timeZone': 'Europe/Tallinn',
      },
      'reminders': {
        'useDefault': False,
        'overrides': [
          {'method': 'popup', 'minutes': 30},
        ],
      },
    }
    event = sdk.events().insert(calendarId='primary', body=event_body).execute()
    print (timestamp() + 'Event created: %s' % (event.get('htmlLink')))
    return

if __name__ == '__main__':
    main()
