# README #

This is a README for my project, facebook-to-calendar (facebookbot.py)

### What is this repository for? ###

I wrote this to integrate Facebook Pages messaging and Google Calendar. 
The use-case is sending a confirmation via messaging from your Page, after which the Python script adds an event in Google Calendar. 

### How do I get set up? ###

1. Setup the Google Calendar SDK by following steps 1 and 2 of the official guide: https://developers.google.com/google-apps/calendar/quickstart/python
2. To enable Graph API for your page, follow the following guide up to step 6 (included): https://developers.facebook.com/docs/pages/getting-started
3. Setup the facebook-sdk for Python: http://facebook-sdk.readthedocs.io/en/latest/install.html
4. In the main directory, create a file facebookkeys.txt in which you paste the Graph API access code on the first line, and your Page ID on the second. 
5. Make sure your Google calendar key is in the file client_secret.json (should be there from step 1)
#### Optional steps follow ####
6. Edit the regex statement and endtime calculation statments in checkCalendarAvailability. 

You can run the script using the following commands: 

usage: facebookbot.py
I suggest running it in a tmux session (since it runs as an infinite loop) after which you can disconnect from the session and leave the script running. 

Tests can be run using the test_facebookbot.py script

Created by Christopher Carr, ITCollege C21