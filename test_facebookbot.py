import unittest
import datetime

from facebookbot import convertDateToDateTime
from facebookbot import convertDateToISO

class FacebookbotTests (unittest.TestCase):
    def test_wrong_format_converts_to_datetime_correctly(self):
        datetime_object = convertDateToDateTime('2018', '12.01', '13:00')	
        self.assertEqual(datetime_object, datetime.datetime(2018, 1, 12, 13, 0))

    def test_datetime_converts_to_iso_correctly(self):
        iso_object = convertDateToISO(datetime.datetime(2018, 2, 27, 9, 0))
        self.assertEqual(iso_object, '2018-02-27T09:00:00+02:00')

    def test_wrong_format_converts_to_iso_correctly(self):
        datetime_object = convertDateToDateTime('2018', '15.03', '23:37')
        iso_object = convertDateToISO(datetime_object)
        self.assertEqual(iso_object, '2018-03-15T23:37:00+02:00')

if __name__ == "__main__":
    unittest.main()
